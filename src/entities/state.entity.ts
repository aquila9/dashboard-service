import "reflect-metadata";
import { injectable } from "inversify";
import winston, { format } from "winston";
import DailyRotateFile = require("winston-daily-rotate-file");
import IState from "../interfaces/state.interface";
import { SkinModel } from "../models/skin.model";
import { HairStyleModel } from "../models/hairStyle.model";
import { ItemModel } from "../models/item.model";
import { AgeModel } from "../models/ageModel";
import { ColorModel } from "../models/color.model";

@injectable()
export default class State implements IState {
  logger: winston.Logger;

  constructor() {
    const { combine, timestamp, printf, label } = winston.format;
    const logFormat = printf(({ level, message, label, timestamp }) => {
      let msg = {
        timestamp,
        level,
        message,
        service: label,
      };

      return JSON.stringify(msg);
    });

    const consoleFormat = format.combine(
      format.colorize(),
      format.timestamp({
        format: "YYYY-MM-DD HH:mm:ss",
      }),
      format.printf(
        (info) =>
          `${info.timestamp} [${process.pid}] ${info.level}: ${info.message}`
      )
    );

    const loggerOptions = {
      file: {
        format: combine(
          timestamp(),
          label({ label: "dashboardService" }),
          logFormat
        ),
        filename:
          process.env.NODE_ENV == "production"
            ? "/etc/logs/backlog_%DATE%.log"
            : "./logs/backlog_%DATE%.log",
        datePattern: "YYYY-MM-DD",
        zippedArchive: false,
        maxSize: "20m",
        maxFiles: "31d",
      },
      console: {
        format: consoleFormat,
        handleExceptions: true,
        json: false,
        colorize: true,
      },
    };

    this.logger = winston.createLogger({
      transports: [
        new DailyRotateFile(loggerOptions.file),
        new winston.transports.Console(loggerOptions.console),
      ],
    });
  }

  async skinMaster() {
    let getAll = await SkinModel.find().lean();

    if (getAll.length == 0) {
      let skinMasterObj = [
        { _id: "6072b4135f66fb9e5e765506", id: 1, name: "White" },
        { _id: "6072b4135f66fb9e5e765508", id: 2, name: "Beige" },
        { _id: "6072b4135f66fb9e5e76550a", id: 3, name: "Brown" },
        { _id: "6072b4135f66fb9e5e76550c", id: 4, name: "Black" },
      ];

      let insert = await SkinModel.collection.insertMany(skinMasterObj);

      if (!insert) {
        this.logger.error("Unable to insert skin master");
      }
    }
  }

  async hairStypeMaster() {
    let getAll = await HairStyleModel.find().lean();
    if (getAll.length == 0) {
      let hairStypeMasterObj = [
        {
          _id: "6072b2e95f66fb9e5e76542d",
          id: 0,
          description: "The bald head",
          name: "bald",
        },
        {
          _id: "6072b2e95f66fb9e5e76542f",
          id: 1,
          description: "The crewcut",
          name: "buzzcut",
        },
        {
          _id: "6072b2e95f66fb9e5e765431",
          id: 2,
          description: "Medium",
          name: "Medium",
        },
        {
          _id: "6072b2e95f66fb9e5e765433",
          id: 3,
          description: "The shaven head",
          name: "Shaven",
        },
        {
          _id: "6072b2e95f66fb9e5e765435",
          id: 4,
          description: "The short hair",
          name: "Short",
        },
        {
          _id: "6072b2e95f66fb9e5e765437",
          id: 5,
          description: "The hairdo",
          name: "Snood",
        },
        {
          _id: "6072b2e95f66fb9e5e765439",
          id: 6,
          description: "The hairs flowing down the shoulder",
          name: "Tress",
        },
      ];
      let insert = await HairStyleModel.collection.insertMany(
        hairStypeMasterObj
      );
      if (!insert) {
        this.logger.error("Unable to insert hair style master");
      }
    }
  }

  async itemMaster() {
    let getAll = await ItemModel.find().lean();
    if (getAll.length == 0) {
      let itemModel = [
        { _id: "6072b8635f66fb9e5e765623", id: 1, name: "Glasses" },
        { _id: "6072b8635f66fb9e5e765625", id: 2, name: "Sunglass" },
        { _id: "6072b8635f66fb9e5e765627", id: 3, name: "Mask" },
        { _id: "6072b8635f66fb9e5e765629", id: 4, name: "Hat" },
      ];
      let insert = await ItemModel.collection.insertMany(itemModel);
      if (!insert) {
        this.logger.error("Unable to insert hair style master");
      }
    }
  }

  async ageMaster() {
    let getAll = await AgeModel.find().lean();
    if (getAll.length == 0) {
      let ageModel = [
        { id: 0, name: "kid" },
        { id: 1, name: "kid_youth" },
        { id: 2, name: "youth" },
        { id: 3, name: "youth_adult" },
        { id: 4, name: "adult" },
        { id: 5, name: "adult_old" },
        { id: 6, name: "old" },
        { id: -1, name: "unknown" },
      ];

      let insert = await AgeModel.collection.insertMany(ageModel);
      if (!insert) {
        this.logger.error("Unable to insert hair style master");
      }
    }
  }

  async color() {
    let getAll = await ColorModel.find().lean();

    if (getAll.length == 0) {
      let colorModel = [
        { id: 0, name: "blue" },
        { id: 1, name: "white" },
        { id: 2, name: "black" },
        { id: 3, name: "pink" },
        { id: 4, name: "red" },
        { id: 5, name: "gray" },
        { id: 6, name: "pattern" },
        { id: 7, name: "purple" },
        { id: 8, name: "striped" },
        { id: 9, name: "green" },
        { id: 10, name: "plaid" },
        { id: 11, name: "orange" },
        { id: 12, name: "yellow" },
        { id: -1, name: "unknown" },
      ];

      let insert = await ColorModel.collection.insertMany(colorModel);
      if (!insert) {
        this.logger.error("Unable to insert color master");
      }
    }
  }

  async init() {
    this.logger.info("Initializing");
    await this.skinMaster();
    await this.hairStypeMaster();
    await this.itemMaster();
    await this.ageMaster();
    await this.color();
    this.logger.info("Init success");
  }
}
