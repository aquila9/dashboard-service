import "reflect-metadata";
import axios from "axios";
import { injectable } from "inversify";
import IYitu from "../interfaces/yitu.interface";
import { UserModel } from "../models/user.model";
import { LoginModel } from "../models/login.model";
import IState from "../interfaces/state.interface";
import container from "../inversify.config";
import { TYPES } from "../types";
import { ResourceModel } from "../models/resource.model";
import { FaceMessageModel, RepositoriesModel } from "models/repositories.model";
import { ChannelModel } from "models/channel.model";
import { StatisticsModel } from "models/statistics.model";
import { DataTrackFace, TrackFaceModel } from "models/trackFace.model";
import { HitRealTimeModel } from "models/hitRealTime.model";
import { DispositionModel } from "models/dispostion.model";
import commonController from "../controllers/common.controller";
import { ChildProcessWithoutNullStreams } from "child_process";

const https = require("https");

const agent = new https.Agent({
  rejectUnauthorized: false,
});

enum method {
  get,
  post,
  put,
  delete,
}
let resultLogin: LoginModel;

@injectable()
export default class Yitu implements IYitu {
  constructor() {}

  async login(
    username?: string,
    password?: string
  ): Promise<LoginModel | undefined> {
    let getLogin = await axios.post(
      `${process.env.YITU_URL}/api/v1/accessor/login`,
      {
        user_name: username || `${process.env.YITU_USERNAME}`,
        password: password || `${process.env.YITU_PASSWORD}`,
      },
      { httpsAgent: agent }
    );

    resultLogin = getLogin.data.data;

    return resultLogin;
  }

  async getResource(): Promise<ResourceModel | undefined> {
    let getResource: ResourceModel = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/system/resource`,
      method.get
    );

    return getResource;
  }

  async getRepositories(): Promise<RepositoriesModel | undefined> {
    let getRepositories: RepositoriesModel = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/repositories`,
      method.get
    );

    return getRepositories;
  }

  async getChannels(
    current: number,
    pageSize: number
  ): Promise<ChannelModel[] | undefined> {
    const dataCurrent: number = current || 1;
    const dataPageSize: number = pageSize || 10000;

    let getChannels: ChannelModel[] = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/system/channels?current=${dataCurrent}&pageSize=${dataPageSize}`,
      method.get
    );

    return getChannels;
  }

  async getStatistics(
    type: string,
    startDate: number,
    endDate: number,
    timeDim: string
  ): Promise<StatisticsModel | undefined> {
    let timeDimData = timeDim || "day";
    let getChannels: StatisticsModel = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/statistics/?type=${type}&start_ts=${startDate}&end_ts=${endDate}&time_dim=${timeDimData}`,
      method.get
    );

    return getChannels;
  }

  async getTrackImage(
    size?: number,
    offset?: number,
    startDate?: number,
    endDate?: number
  ): Promise<TrackFaceModel | undefined> {
    let sizeData = size || 1000000;
    let offsetData = 0;
    let channels: string = "";

    let getChanels = await this.getChannels(1, 10000);

    if (getChanels) {
      for (const data of getChanels) {
        channels += `,${data.id}`;
      }
    }

    let a = `${process.env.YITU_URL}/vbox/v1/face_images/track_face?size=${
      process.env.MAX_SIZE_FACE
    }&offset=${offsetData}&start_time=${startDate}&end_time=${endDate}&channel_ids=${channels.substring(
      1
    )}`;

    let getTrackFace: TrackFaceModel = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/face_images/track_face?size=${
        process.env.MAX_SIZE_FACE
      }&offset=${offsetData}&start_time=${startDate}&end_time=${endDate}&channel_ids=${channels.substring(
        1
      )}`,
      method.get
    );

    return getTrackFace;
  }

  async getAllDisposition(): Promise<DispositionModel[] | undefined> {
    let getDisposition: DispositionModel[] = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/dispositions?disposition_id=-1`,
      method.get
    );

    return getDisposition;
  }

  async getRealTimeAlarm(
    createDate: Date
  ): Promise<HitRealTimeModel | undefined> {
    let createdDate: number = +createDate;

    let disposition: string = "";
    let getDisposition: DispositionModel[] | undefined =
      await this.getAllDisposition();

    if (getDisposition) {
      for (const data of getDisposition) {
        disposition += `,${data.disposition_id}`;
      }
    }

    let gethitRealTime: HitRealTimeModel = await this.execute(
      `${
        process.env.YITU_URL
      }/vbox/v1/track/hit_realtime?size=50&timestamp=${createdDate}&disposition_ids=${disposition.substring(
        1
      )}`,
      method.get
    );

    return gethitRealTime;
  }

  async getFaceInformation(
    startTimeStamp: number,
    endTimeStamp: number
  ): Promise<any | null> {
    try {
      let realTimeData: any[] = [];
      let channels: string = "";

      let disposition: string = "";
      let getDisposition: DispositionModel[] | undefined =
        await this.getAllDisposition();

      if (getDisposition) {
        for (const data of getDisposition) {
          disposition += `,${data.disposition_id}`;
        }
      }

      // * Find channel name
      let getChanels = await this.getChannels(1, 10000);

      if (getChanels) {
        for (const data of getChanels) {
          channels += `,${data.id}`;
        }
      }

      let getTrackHitFetch = await this.execute(
        `${process.env.YITU_URL}/vbox/v1/track/hit_fetch?size=${
          process.env.MAX_SIZE_FACE
        }&offset=0&start_time=${Math.floor(
          startTimeStamp / 1000
        )}&end_time=${Math.floor(
          endTimeStamp / 1000
        )}&is_hit=0,1&channel_ids=${channels.substring(
          1
        )}&disposition_ids=${disposition.substring(1)}`,
        method.get
      );

      if (!getTrackHitFetch.hits) {
        return null;
      }

      let getRepoId = await this.execute(
        `${process.env.YITU_URL}/vbox/v1/repositories`,
        method.get
      );

      for (let hitFetch of getTrackHitFetch.hits) {
        let getTrackBatch = await this.execute(
          `${process.env.YITU_URL}/vbox/v1/face_images/track_batch?face_ids=${hitFetch.face_id}`,
          method.get
        );
        let getBatch = await this.execute(
          `${process.env.YITU_URL}/vbox/v1/face_images/batch?face_ids=${hitFetch.hit_face_id}`,
          method.get
        );

        if (getTrackBatch) {
          for (let trackBatch of getTrackBatch) {
            realTimeData.push({
              channelName: getChanels?.filter(
                (o) => o.id == hitFetch.channel_id
              )[0].name,
              hitFaceId: hitFetch.hit_face_id,
              createdDate: new Date(+hitFetch.timestamp * 1000),
              disposition_name: hitFetch.disposition_name,
              // address: facImage.address,
              image: getBatch
                ? `${process.env.YITU_URL}/vbox/v1/image_storage/http_image?uri=${getBatch[0].face_image_uri}`
                : undefined,
              scene_image_uri: `${process.env.YITU_URL}/vbox/v1/image_storage/http_image?uri=${trackBatch.scene_image_uri}`,
              // gender: facImage.gender,
              // name: facImage.name,
              // repository: facImage.repository_id,
              personType: {
                id: hitFetch.hit_repository_id,
                name: getRepoId
                  ? getRepoId.repositories.filter(
                      (o: any) => o.id == hitFetch.hit_repository_id
                    ).length > 0
                    ? getRepoId.repositories.filter(
                        (o: any) => o.id == hitFetch.hit_repository_id
                      )[0].name
                    : undefined
                  : undefined,
              },
              meta: trackBatch.meta,
              similarity: hitFetch.similarity,
              name: getBatch ? getBatch[0].name : undefined,
            });
          }
        }
      }

      return realTimeData;
    } catch (error) {
      console.log(error.message);
      throw error;
    }
  }

  async getTrackHitFetch(
    startDate: number,
    endDate: number,
    channel: string,
    disposition: string
  ): Promise<any | undefined> {
    let getFaceImage = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/face_images/track_face?size=24&offset=0&start_time=${startDate}&end_time=${endDate}`,
      method.get
    );

    let getTrackHitFetch = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/track/hit_fetch?size=${
        process.env.MAX_SIZE_FACE
      }&offset=0&start_time=${startDate}&end_time=${endDate}&is_hit=0,1&channel_ids=${channel.substring(
        1
      )}&disposition_ids=${disposition.substring(1)}`,
      method.get
    );

    return getTrackHitFetch;
  }

  async getTrackFace(startDate: number, endDate: number) {
    let getFaceImage = await this.execute(
      `${process.env.YITU_URL}/vbox/v1/face_images/track_face?size=${
        process.env.MAX_SIZE_FACE
      }&offset=0&start_time=${startDate / 1000}&end_time=${endDate / 1000}`,
      method.get
    );

    let getChanels = await this.getChannels(1, 10000);

    if (getFaceImage) {
      if (getFaceImage.data) {
        if (getFaceImage.data.length > 0) {
          for (const result of getFaceImage.data) {
            result.channelName = getChanels?.filter(
              (o) => o.id == result.channel_id
            )[0].name;
          }
        }
      }
    }

    return getFaceImage;
  }

  async getFaceImage(hitFaceId: string): Promise<any | undefined> {
    let getFaceImage = await this.execute(
      `${
        process.env.YITU_URL
      }/vbox/v1/face_images/batch?face_ids=${hitFaceId.substring(1)}`,
      method.get
    );

    return getFaceImage;
  }

  async getTrackBatch(faceId: string) {
    let getTrackBatch = await this.execute(
      `${
        process.env.YITU_URL
      }/vbox/v1/face_images/track_batch?face_ids=${faceId.substring(1)}`,
      method.get
    );

    return getTrackBatch;
  }

  async execute(
    url: string,
    method: method,
    data?: any
  ): Promise<any | undefined> {
    if (!resultLogin) {
      await this.login();
    }

    let config: any = {
      method: method,
      url: url,
      httpsAgent: agent,
      headers: {
        Authorization: `Bearer ${resultLogin.access_token}`,
      },
      data: data,
    };

    try {
      let execute = await axios(config);

      if (execute.status == 200) {
        if (execute.data == null) {
          return null;
        }

        if (execute.data.rtn != 0) {
          await this.login();
          await this.execute(url, method, data);
        }
      }

      return execute.data.data;
    } catch (error) {
      console.log(error.message);
      return null;
    }
  }
}
