import yituController from "../controllers/yitu.controller";
import { injectable } from "inversify";
import ISchedule from "../interfaces/schedule.interface";
import container from "../inversify.config";
import IState from "interfaces/state.interface";
import { TYPES } from "../types";
import "reflect-metadata";
import notificationController from "../controllers/notification.controller";
import settingController from "../controllers/setting.controller";
import commonController from "../controllers/common.controller";
import axios from "axios";
import { SettingModel } from "../models/setting.model";
import lprController from "../controllers/lpr.controller";
import { LprModel } from "../models/lpr.model";
const nodeSchedule = require("node-schedule");

let state: any;
@injectable()
export default class Schedule implements ISchedule {
  constructor() {}

  async syncDataFace(cornJob: string) {
    nodeSchedule.scheduleJob(cornJob, async () => {
      state = container.get<IState>(TYPES.State);
      state.logger.info("Syncing face data");

      let startDateNow = new Date();
      let startDate = startDateNow.setHours(
        startDateNow.getHours(),
        startDateNow.getMinutes() - 1,
        startDateNow.getSeconds(),
        startDateNow.getMilliseconds()
      );

      let endDateNow = new Date();
      let endDate = endDateNow.setHours(
        endDateNow.getHours(),
        endDateNow.getMinutes(),
        endDateNow.getSeconds(),
        endDateNow.getMilliseconds()
      );

      state.logger.info(`Startdate: ${startDate} Enddate : ${endDate}`);

      let result = await yituController.syncDataFace(+startDate, +endDate);

      if (!result) {
        state.logger.info("No information at this time");
      } else {
        state.logger.info(`Information this time : ${result.length}`);
      }
    });
  }

  async alertFace(cornJob: string) {
    nodeSchedule.scheduleJob(cornJob, async () => {
      state.logger.info("Start alert get face data");
      state = container.get<IState>(TYPES.State);
      try {
        let startDate = new Date();

        startDate.setHours(
          startDate.getHours(),
          startDate.getMinutes() - 1,
          startDate.getSeconds(),
          startDate.getMilliseconds()
        );

        let endDate = new Date();

        let getSetting = await SettingModel.findById(
          "5fa10d1f20cb5050d43c1991"
        ).lean();

        if (!getSetting) {
          state.logger.error("Unable to setting notification");
          return;
        }

        if (!getSetting.endDate) {
          state.logger.info("endDate not setting");
          return;
        }

        if (startDate <= new Date(+getSetting.endDate)) {
          state.logger.info(`Getting face data`);

          state.logger.info(`StartDate: ${startDate} - EndDate: ${endDate}`);
          let data = await yituController.getDataFace(startDate, endDate, 1);

          if (!data) {
            state.logger.info("Unable to get data face");
          }

          if (getSetting.type) {
            data.filter((o) => o.personType.id == getSetting?.type);
          }

          if (getSetting.age) {
            data.filter((o) => o.meta.age.id == getSetting?.age);
          }

          if (getSetting.skin) {
            data.filter((o) => o.meta.skin_color.id == getSetting?.skin);
          }

          if (getSetting.hairStyle) {
            data.filter((o) => o.meta.skin_color.id == getSetting?.skin);
          }

          if (getSetting.gender) {
            data.filter((o) => o.meta.gender.id == getSetting?.gender);
          }

          if (getSetting.glass) {
            data.filter((o) => o.meta.sunglass == getSetting?.glass);
          }

          if (getSetting.sunglass) {
            data.filter((o) => o.meta.sunglass == getSetting?.sunglass);
          }

          if (getSetting.mask) {
            data.filter((o) => o.meta.mask == getSetting?.mask);
          }

          if (getSetting.hat) {
            data.filter((o) => o.meta.hat == getSetting?.hat);
          }

          let obj: any[] = [];

          state.logger.info(JSON.stringify(data));
          for (const result of data) {
            obj.push(result.disposition_name);
          }

          state.logger.info(`Count face ${obj}`);
          let groupCount = obj.reduce((total, value) => {
            total[value] = (total[value] || 0) + 1;
            return total;
          }, {});

          state.logger.info(`${JSON.stringify({ ...groupCount })}`);

          let chkArray: any[] = [];

          for (let resultObj of obj) {
            let chk = chkArray.filter((o) => o == resultObj);

            if (chk.length == 0) {
              await notificationController.alertLine(
                settingController.typeSetting.face,
                `${resultObj} ${groupCount[resultObj]}  `
              );

              chkArray.push(resultObj);
            }
          }
        }
      } catch (error) {
        state.logger.error("Function alert data face" + error.message);
        return;
      }
    });
  }

  async alertLpr(cornJob: string) {
    nodeSchedule.scheduleJob(cornJob, async () => {
      state = container.get<IState>(TYPES.State);
      state.logger.info("Start alert get lpr data");

      let startDate = new Date();

      startDate.setHours(
        startDate.getHours(),
        startDate.getMinutes() - 1,
        startDate.getSeconds(),
        startDate.getMilliseconds()
      );

      let endDate = new Date();

      let getSetting = await SettingModel.findById(
        "5fa10d1f20cb5050d43c1992"
      ).lean();

      if (!getSetting) {
        state.logger.error("Unable to setting notification");
        return;
      }

      if (!getSetting.endDate) {
        state.logger.info("endDate not setting");
        return;
      }

      let maxSizepLpr = process.env.MAX_SIZE_LPR!;

      if (startDate <= new Date(+getSetting.endDate)) {
        state.logger.info(`Getting lpr data`);
        let getLpr = await lprController.getLprDashboard(
          +startDate,
          +endDate,
          +maxSizepLpr,
          1
        );

        if (getSetting.make) {
          getLpr.filter((o: any) => o.make == getSetting?.make);
        }

        if (getSetting.vehicleColor) {
          getLpr.filter((o: any) => o.vehicleColor == getSetting?.make);
        }

        if (getSetting.plateNumber) {
          getLpr.filter((o: any) => o.plateNumber == getSetting?.plateNumber);
        }

        if (getSetting.generation) {
          getLpr.filter((o: any) => o.generation == getSetting?.generation);
        }

        state.logger.info(`Count lpr ${getLpr.length}`);
        await notificationController.alertLine(
          settingController.typeSetting.lpr,
          getLpr.length
        );
      }
    });
  }

  async clearDate(cornJob: string = "") {
    nodeSchedule.scheduleJob(cornJob, async () => {
      state = container.get<IState>(TYPES.State);
      const pass20Date = new Date(Date.now() - 60 * 60 * 24 * 20 * 1000);

      let update = await LprModel.updateMany(
        { createdDate: { $lt: pass20Date } },
        { platePicture: undefined, sceenPicture: undefined }
      );

      if (!update) {
        state.logger.error("Unable to clear platePicture and sceenPicture");
      }

      state.logger.info("Success for clear platePicture and sceenPicture");
    });
  }
}
