import "reflect-metadata";
import { injectable } from "inversify";
import IAuthentication from "interfaces/authentication.interface";

@injectable()
export default class Authentication implements IAuthentication {
  constructor() {}

  async authenJWT(req: any, res: any, next: any): Promise<string> {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
      return res.sendStatus(401);
    }

    ///TODO : Recheck with yity box

    next();
    return "Hello world";
  }
}
