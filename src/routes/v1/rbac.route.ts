import express from "express";
import { UserModel } from "../../models/user.model";
import IState from "../../interfaces/state.interface";
import container from "../../inversify.config";
import { TYPES } from "../../types";

var router = express.Router();
let state = container.get<IState>(TYPES.State);

router.get("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.get("/:id", (req, res) => {
  try {
  } catch (error) {
    state.logger.error(error.message);
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.post("/", async (req, res) => {
  try {
    let body = req.body;

    if (!body) {
      res.status(400).respond(-1, "Body is empty", null);
      return;
    }

    let check = await UserModel.find({
      username: body.username,
      password: body.password,
    });

    if (check.length == 0) {
      res.status(400).respond(-1, "Unable to login", null);
      return;
    }

    res.status(200).respond(0, "Success", null);
    return;
  } catch (error) {
    state.logger.error(error.message);
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.put("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.delete("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

export default router;
