import yituController from "../../controllers/yitu.controller";
import Yitu from "entities/yitu.entity";
import express from "express";
import IState from "interfaces/state.interface";
import { RepositoriesModel } from "models/repositories.model";
import { DataTrackFace } from "models/trackFace.model";
import { Types } from "mongoose";
import IYitu from "../../interfaces/yitu.interface";
import container from "../../inversify.config";
import { TYPES } from "../../types";
import commonController from "../../controllers/common.controller";
import { SnapshotModel } from "../../models/snapshot.model";

var router = express.Router();
let state = container.get<IState>(TYPES.State);
let yitu = container.get<IYitu>(TYPES.Yitu);

router.get("", async (req, res) => {
  try {
    const { startDate, endDate } = req.query;

    if (!startDate || !endDate) {
      res.status(400).respond(0, "StartDate or EndDate is empty", null);
      return;
    }

    // let data = await SnapshotModel.find({
    //   createdDate: {
    //     $gte: commonController.setTimeStart(+startDate),
    //     $lt: commonController.setTimeEnd(+endDate),
    //   },
    // });

    // res.status(200).respond(0, "Success", data);

    //#region Comment
    // if (!startDate || !endDate) {
    //   res.status(400).respond(0, "StartDate or EndDate is empty", null);
    //   return;
    // }

    let data: any[] = [];
    data = await yituController.getDataFace(
      commonController.setTimeStart(+startDate),
      commonController.setTimeEnd(+endDate),
      2
    );

    // if (data.length == 0) {
    //   console.log(commonController.setTimeStart(+startDate));
    //   console.log(commonController.setTimeEnd(+endDate));
    //   data = await SnapshotModel.find({
    //     createdDate: {
    //       $gte: commonController.setTimeStart(+startDate),
    //       $lt: commonController.setTimeEnd(+endDate),
    //     },
    //   });
    // }

    res.status(200).respond(0, "Success", data);
    //#endregion
    return;
  } catch (error) {
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.get("/realTime", async (req, res) => {
  try {
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;

    if (!startDate || !endDate) {
      res.status(400).respond(0, "StartDate or EndDate is empty", null);
      return;
    }

    let data = await yituController.getDataFace(
      commonController.setTimeStart(+startDate),
      commonController.setTimeEnd(+endDate),
      1
    );

    res.status(200).respond(0, "Success", data);
    return;
  } catch (error) {
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

function filterData(array: any, key: string, value: any): any {
  if (value != -99) {
    return array.filter((e: any) => {
      return e[key] == value;
    });
  }
}

export default router;
