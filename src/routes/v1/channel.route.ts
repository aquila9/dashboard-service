import express from "express";
import { TYPES } from "../../types";
import IYitu from "../../interfaces/yitu.interface";
import container from "../../inversify.config";
import IState from "../../interfaces/state.interface";

var router = express.Router();
let yitu = container.get<IYitu>(TYPES.Yitu);
let state = container.get<IState>(TYPES.State);

router.get("/", async (req, res) => {
  try {
    let getChannels = await yitu.getChannels();

    if (!getChannels) {
      res.status(404).respond(-1, "Not found", null);
      return;
    }

    res.status(200).respond(0, "Success", getChannels);

    return;
  } catch (error) {
    res.status(500).respond(99, error.message, null);
    return;
  }
});

export default router;
