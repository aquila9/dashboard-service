import express from "express";
import { TYPES } from "../../types";
import IState from "../../interfaces/state.interface";
import container from "../../inversify.config";
import mongoDB from "../../server";
import commonController from "../../controllers/common.controller";
import { LprModel } from "../../models/lpr.model";
import lprController from "../../controllers/lpr.controller";
import IYitu from "../../interfaces/yitu.interface";
import { LprDashboardModel } from "../../models/lprDashboard.model";
import { info } from "winston";

const mysql = require("mysql");
var router = express.Router();
let state = container.get<IState>(TYPES.State);
let yitu = container.get<IYitu>(TYPES.Yitu);
let mysqlDB: any;

router.get("/", async (req, res) => {
  try {
    let {
      startDate,
      endDate,
      country,
      make,
      model,
      vehicleColor,
      plateNumber,
      vehicleList,
      laneName,
    } = req.query;

    let page = req.query.page == "0" ? 1 : req.query.page || 1;
    let limit = req.query.limit == "0" ? 10 : req.query.limit || 10;

    if (!startDate || !endDate) {
      res.status(400).respond(-1, "StartDate or endDate is empty", null);
      return;
    }

    let obj: any = {};

    obj = {
      createdDate: {
        $gte: commonController.setTimeStart(+startDate),
        $lte: commonController.setTimeEnd(+endDate),
      },
    };

    if (country) {
      obj.country = country;
    }

    if (make) {
      obj.make = make;
    }

    if (model) {
      obj.model = model;
    }

    if (vehicleColor) {
      obj.vehicleColor = vehicleColor;
    }

    if (plateNumber) {
      obj.plateNumber = plateNumber;
    }

    if (vehicleList) {
      obj.vehicleList = vehicleList;
    }

    if (laneName) {
      obj.laneName = laneName;
    }

    let getLengthLpr = await lprController.getLpr(obj, +limit, +page);

    res.status(200).respond(0, "Success", getLengthLpr);
    return;
  } catch (error) {
    state.logger.error(error.message);
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.get("/normal", async (req, res) => {
  try {
    state.logger.info("Start function lpr");
    let limit = `${process.env.MAX_SIZE_LPR! || 100}`;
    let feild = req.query.feild || "";

    if (!feild) {
      let getLpr = await LprModel.find()
        .limit(+limit)
        .lean();

      res.status(200).respond(0, "Success", getLpr);
      return;
    }

    let getLpr = await LprModel.find()
      .limit(+limit)
      .distinct(feild.toString())
      .lean();

    res.status(200).respond(0, "Success", getLpr);
    return;
  } catch (err) {
    res.status(400).respond(-1, err.message, null);
    return;
  }
});

router.get("/dashboard", async (req, res) => {
  try {
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;

    let page = req.query.page == "0" ? 1 : req.query.page || 1;
    let limit = req.query.limit == "0" ? 10 : process.env.MAX_SIZE_LPR!;

    if (!startDate || !endDate) {
      res.status(400).respond(-1, "StartDate or endDate is empty", null);
      return;
    }

    let dataResponse: any = [];

    let getLengthLpr = await lprController.getLprDashboard(
      +startDate,
      +endDate,
      +limit,
      +page
    );

    res.status(200).respond(0, "Success", getLengthLpr);
    return;
  } catch (error) {
    state.logger.error(error.message);
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.get("/:id", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.post("/", async (req, res) => {
  try {
    let body = req.body;

    if (!body) {
      res.status(400).respond(-1, "Body is empty", null);
      return;
    }

    body.createdDate = new Date(+body.createdDate);
    body.platePicture = `data:image/png;base64,${body.platePicture}`;
    body.sceenPicture = `data:image/png;base64,${body.sceenPicture}`;

    if (body.make == "Ford" && body.model == "Explorer") {
      body.make = "Isuzu";
    }

    let insert = await LprModel.collection.insertOne(body);

    if (!insert) {
      state.logger.error("Unable to insert lpr");
      res.status(400).respond(-1, "Unable to insert lpr", null);
      return;
    }

    res.status(200).respond(0, "Success", null);
    return;
  } catch (error) {
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.post("/dashboard", async (req, res) => {
  try {
    let body = req.body;

    if (!body) {
      res.status(400).respond(-1, "Body is empty", null);
      return;
    }

    body.createdDate = new Date(+body.createdDate);
    if (body.make == "Ford" && body.model == "Explorer") {
      body.make = "Isuzu";
      body.model = "";
    }

    if (!body.vehicleColor) {
      body.vehicleColor = "unknown";
    }

    let insert = await LprDashboardModel.collection.insertOne(body);

    if (!insert) {
      state.logger.error("Unable to insert lpr");
      res.status(400).respond(-1, "Unable to insert lpr", null);
      return;
    }

    res.status(200).respond(0, "Success", null);
    return;
  } catch (error) {
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.put("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.delete("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

// * for update ford explorer to isuzu
router.get("/update/lpr", async (req, res) => {
  let update1 = await LprModel.collection.updateMany(
    {
      vehicleColor: "Unknown",
    },
    {
      $set: { vehicleColor: "unknown" },
    }
  );
  let update = await LprModel.collection.updateMany(
    {
      vehicleColor: "",
    },
    {
      $set: { vehicleColor: "unknown" },
    }
  );

  if (!update) {
    state.logger.error("Update not success");
  }
  state.logger.info("Update success");

  res.status(200).respond(-1, "Success", null);
  return;
});

export default router;
