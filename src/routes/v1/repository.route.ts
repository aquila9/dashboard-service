import express from "express";
import IAuthentication from "interfaces/authentication.interface";
import IYitu from "../../interfaces/yitu.interface";
import container from "../../inversify.config";
import { TYPES } from "../../types";

var router = express.Router();

let yitu = container.get<IYitu>(TYPES.Yitu);
let authen = container.get<IAuthentication>(TYPES.Authen);

router.get("/", async (req, res) => {
  try {
    let getRepository = await yitu.getRepositories();

    if (!getRepository) {
      res.status(404).respond(-1, "Not found", null);
      return;
    }

    res.status(200).respond(0, "Success", getRepository);

    return;
  } catch (error) {
    res.status(500).respond(99, error.message, null);
    return;
  }
});

export default router;
