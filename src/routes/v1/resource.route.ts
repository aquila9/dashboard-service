import express from "express";
import IYitu from "../../interfaces/yitu.interface";
import container from "../../inversify.config";
import { TYPES } from "../../types";

let yitu = container.get<IYitu>(TYPES.Yitu);

var router = express.Router();

router.get("/", async (req, res) => {
  try {
    let getResources = await yitu.getResource();

    res.status(200).respond(0, "Success", getResources);
    return;
  } catch (error) {
    res.status(500).respond(99, error.message, null);
    return;
  }
});

export default router;
