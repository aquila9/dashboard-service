import express from "express";
import container from "../../inversify.config";
import { getuid } from "process";
import { SettingModel } from "../../models/setting.model";
import IState from "../../interfaces/state.interface";
import { TYPES } from "../../types";
import { SnapshotModel } from "../../models/snapshot.model";
import settingController from "../../controllers/setting.controller";

var router = express.Router();
let state = container.get<IState>(TYPES.State);
var ObjectId = require("mongoose").Types.ObjectId;

router.get("/", async (req, res) => {
  try {
    let type = req.query.type;

    if (!type) {
      res.status(400).respond(-1, "Type is empty", null);
      return;
    }

    switch (+type) {
      case settingController.typeSetting.face:
        let getSetting = await SettingModel.findById(
          "5fa10d1f20cb5050d43c1991"
        );

        res.status(200).respond(0, "Success", getSetting);
        return;
      case settingController.typeSetting.lpr:
        let getSettingLpr = await SettingModel.findById(
          "5fa10d1f20cb5050d43c1992"
        );

        res.status(200).respond(0, "Success", getSettingLpr);
        return;
    }

    // switch (+type) {
    //   case settingController.typeSetting.face:
    //     let getFace = await settingController.getFaceBySetting();

    //     res.status(200).respond(0, "Success", getFace);
    //     return;
    //   case settingController.typeSetting.lpr:
    //     let getLpr = settingController.getLprBySetting();

    //     res.status(200).respond(0, "Success", getLpr);
    //     return;
    // }
  } catch (error) {
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.get("/:id", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.post("/", async (req, res) => {
  try {
    let body = req.body;
    let type = req.query.type || 0;

    if (!type || !body) {
      state.logger.error("Type or body is empty");
      return;
    }

    switch (+type) {
      case settingController.typeSetting.face:
        settingController.settingFace(body);
        break;
      case settingController.typeSetting.lpr:
        settingController.settingLpr(body);
        break;
    }

    res.status(200).respond(0, "Success", null);
    return;
  } catch (err) {
    res.status(400).respond(-1, err.message, null);
    return;
  }
});

router.put("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.delete("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

export default router;
