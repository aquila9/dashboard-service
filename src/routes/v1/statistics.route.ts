import express from "express";
import { RealTimeModel } from "../../models/realTime.model";
import { SnapshotModel } from "../../models/snapshot.model";
import IState from "../../interfaces/state.interface";
import IYitu from "../../interfaces/yitu.interface";
import container from "../../inversify.config";
import { TYPES } from "../../types";
import yituController from "../../controllers/yitu.controller";
import commonController from "../../controllers/common.controller";

let yitu = container.get<IYitu>(TYPES.Yitu);
let state = container.get<IState>(TYPES.State);
var router = express.Router();

router.get("/", async (req, res) => {
  try {
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;

    if (!startDate || !endDate) {
      res.status(400).respond(-1, "Start date and end date is empty", null);
      return;
    }
    let getSnapShot = await yituController.getDataFace(
      commonController.setTimeStart(+startDate),
      commonController.setTimeEnd(+endDate),
      2
    );

    let getAlert = await yituController.getDataFace(
      commonController.setTimeStart(+startDate),
      commonController.setTimeEnd(+endDate),
      1
    );

    res.status(200).respond(0, "Success", {
      snapshot: getSnapShot || [],
      alert: getAlert || [],
    });
    return;
  } catch (error) {
    state.logger.error(error.message);
    res.status(200).respond(99, error.message, null);
    return;
  }
});

router.get("/:id", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.post("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.put("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.delete("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

export default router;
