import express from "express";
import IAuthentication from "interfaces/authentication.interface";
import IYitu from "../../interfaces/yitu.interface";
import container from "../../inversify.config";
import { TYPES } from "../../types";

let yitu = container.get<IYitu>(TYPES.Yitu);
let authen = container.get<IAuthentication>(TYPES.Authen);

var router = express.Router();

export default router;
