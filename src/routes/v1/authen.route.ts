import Yitu from "entities/yitu.entity";
import express from "express";
import { Authen, AuthenModel } from "../../models/authen.model";
import IYitu from "../../interfaces/yitu.interface";
import container from "../../inversify.config";
import { TYPES } from "../../types";

const jwt = require("jsonwebtoken");
var router = express.Router();
let yitu = container.get<IYitu>(TYPES.Yitu);
router.get("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.get("/:id", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.post("/", async (req, res) => {
  try {
    let body = req.body;

    if (!body) {
      res.status(400).respond(-1, "Unable to login", null);
      return;
    }

    //let login = await yitu.login();

    let authen: Authen = {
      username: body.username,
      createdOn: new Date(),
    };

    let insertHistory = await AuthenModel.collection.insertOne(authen);

    if (!insertHistory) {
      res.status(400).respond(-1, "Uable to login", null);
      return;
    }

    const accessToken = jwt.sign({ username: body.username }, "key");

    res.status(200).respond(0, "Success", accessToken);
    return;
  } catch (error) {
    res.status(500).respond(99, error.message, null);
    return;
  }
});

router.put("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.delete("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

export default router;
