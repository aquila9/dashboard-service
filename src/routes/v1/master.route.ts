import express from "express";
import { SkinModel } from "../../models/skin.model";
import { HairStyleModel } from "../../models/hairStyle.model";
import { ItemModel } from "../../models/item.model";
import container from "../../inversify.config";
import IYitu from "../../interfaces/yitu.interface";
import { TYPES } from "../../types";
import { AgeModel } from "../../models/ageModel";
import { ColorModel } from "../../models/color.model";

var router = express.Router();
let yitu = container.get<IYitu>(TYPES.Yitu);

router.get("/type", async (req, res) => {
  try {
    let getType = await yitu.getRepositories();

    if (!getType) {
      res.status(200).respond(0, "Success", null);
      return;
    }

    res.status(200).respond(0, "Success", getType.repositories);
    return;
  } catch (error) {
    res.status(500).respond(-1, error.message, null);
    return;
  }
});

router.get("/hairStyle", async (req, res) => {
  try {
    let getAll = await HairStyleModel.find().lean();

    res.status(200).respond(0, "Success", getAll);
    return;
  } catch (error) {
    res.status(500).respond(-1, error.message, null);
    return;
  }
});

router.get("/skin", async (req, res) => {
  try {
    let getAll = await SkinModel.find().lean();

    res.status(200).respond(0, "Success", getAll);
    return;
  } catch (error) {
    res.status(500).respond(-1, error.message, null);
    return;
  }
});

router.get("/item", async (req, res) => {
  try {
    let getAll = await ItemModel.find().lean();

    res.status(200).respond(0, "Success", getAll);
    return;
  } catch (error) {
    res.status(500).respond(-1, error.message, null);
    return;
  }
});

router.get("/age", async (req, res) => {
  try {
    let getAll = await AgeModel.find().lean();

    res.status(200).respond(0, "Success", getAll);
    return;
  } catch (error) {
    res.status(500).respond(-1, error.message, null);
    return;
  }
});

router.get("/color", async (req, res) => {
  try {
    let getAll = await ColorModel.find().lean();

    res.status(200).respond(0, "Success", getAll);
    return;
  } catch (error) {
    res.status(500).respond(-1, error.message, null);
    return;
  }
});

export default router;
