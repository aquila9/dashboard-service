import axios from "axios";
import settingController from "../../controllers/setting.controller";
import express from "express";
import container from "../../inversify.config";
import IState from "../../interfaces/state.interface";
import { TYPES } from "../../types";
import { stat } from "fs";

let state = container.get<IState>(TYPES.State);
var router = express.Router();

router.get("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.get("/:id", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.post("/", (req, res) => {
  try {
    let type = req.query.type;
    let body = req.body;

    if (!type) {
      res.status(400).respond(-1, "Type is empty", null);
      return;
    }

    if (+body.count == 0) {
      res.status(200).respond(0, "Successs", null);
      return;
    }

    switch (+type) {
      case settingController.typeSetting.face:
        state.logger.info(`${process.env.LINE_URL}/face?id=${body.count}`);

        axios
          .get(`${process.env.LINE_URL}/face?id=${body.count}`, {
            timeout: 5 * 1000,
          })
          .then((obj) => {
            state.logger.info("Send line success");
            res.status(200).respond(0, "Successs", null);
            return;
          })
          .catch((err) => {
            state.logger.error(err.message);
            res.status(200).respond(0, "Successs", null);
            return;
          });
        return;
      case settingController.typeSetting.lpr:
        state.logger.info(`${process.env.LINE_URL}/lpr?id=${body.count}`);
        axios
          .get(`${process.env.LINE_URL}/lpr?id=${body.count}`)
          .then((obj) => {
            state.logger.info("Send line success");
            res.status(200).respond(0, "Successs", null);
            return;
          })
          .catch((err) => {
            state.logger.error(err.message);
            res.status(200).respond(0, "Successs", null);
            return;
          });
        return;
    }

    res.status(200).respond(0, "Successs", null);
    return;
  } catch (error) {
    res.status(400).respond(-1, error.message, null);
    return;
  }
});

router.put("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

router.delete("/", (req, res) => {
  res.status(500).respond(99, "Not implemented yet", null);
});

export default router;
