import { LprDashboardModel } from "../models/lprDashboard.model";
import { LprModel } from "../models/lpr.model";
import commonController from "./common.controller";

async function getLpr(obj: any, limit: number, page: number) {
  let dataResponse: any = {};

  let getLpr = await LprModel.find({ ...obj })
    .sort({ createdDate: -1 })
    .skip((+page - 1) * +limit)
    .limit(limit);

  dataResponse._metadata = {
    total: 1000000,
    page: page,
    pages: 1000000 / 10,
  };

  dataResponse.lpr = getLpr;

  // let getLpr = await LprModel.aggregate([
  //   {
  //     $match: {
  //       ...obj,
  //     },
  //   },
  //   {
  //     $sort: { createdDate: -1 },
  //   },
  //   {
  //     $facet: {
  //       _metadata: [
  //         { $count: "total" },
  //         {
  //           $addFields: {
  //             page: page,
  //             pages: { $ceil: { $divide: [`$total`, +limit] } },
  //           },
  //         },
  //       ],
  //       lpr: [{ $skip: (+page - 1) * +limit }, { $limit: +limit }],
  //     },
  //   },
  //   {
  //     $project: {
  //       _metadata: { $arrayElemAt: ["$_metadata", 0] },
  //       lpr: 1,
  //     },
  //   },
  // ]).allowDiskUse(true).pretty();

  // dataResponse._metadata = getLpr[0]._metadata || {
  //   total: 0,
  //   page: 1,
  //   pages: 1,
  // };

  // dataResponse.lpr = getLpr;

  return dataResponse;
}

async function getLprDashboard(
  startDate: number,
  endDate: number,
  limit: number,
  page: number
) {
  let getLpr = await LprDashboardModel.find({
    createdDate: {
      $gte: commonController.setTimeStart(+startDate),
      $lte: commonController.setTimeEnd(+endDate),
    },
  }).lean();

  // {
  //   $sort: { createdDate: -1 },
  // },
  // {
  //   $facet: {
  //     _metadata: [
  //       { $count: "total" },
  //       {
  //         $addFields: {
  //           page: page,
  //           pages: { $ceil: { $divide: [`$total`, +limit] } },
  //         },
  //       },
  //     ],
  //     lpr: [{ $skip: (+page - 1) * +limit }, { $limit: +limit }],
  //   },
  // },
  // {
  //   $project: {
  //     _metadata: { $arrayElemAt: ["$_metadata", 0] },
  //     lpr: 1,
  //   },
  // },

  return getLpr;
}

const lprController = {
  getLpr,
  getLprDashboard,
};

export default lprController;
