import { SettingModel } from "../models/setting.model";
import IState from "../interfaces/state.interface";
import container from "../inversify.config";
import { TYPES } from "../types";
import { SnapshotModel } from "../models/snapshot.model";
import { Lpr, LprModel } from "../models/lpr.model";

//let state = container.get<IState>(TYPES.State);
var ObjectId = require("mongoose").Types.ObjectId;

enum typeSetting {
  face = 1,
  lpr = 2,
}

async function settingFace(obj: any): Promise<boolean> {
  if (!obj) {
    //state.logger.info("Object is empty");
    return false;
  }

  let _id = new ObjectId("5fa10d1f20cb5050d43c1991");

  let getById = await SettingModel.findById(_id);

  if (getById) {
    let remove = await SettingModel.findByIdAndRemove(_id);

    if (!remove) {
      //state.logger.error("Unable to remove");
      return false;
    }
  }

  obj._id = _id;
  let insert = await SettingModel.collection.insertOne({ ...obj });

  if (!insert) {
    //state.logger.info("Unable to insert setting face");
    return false;
  }

  let objFilter: any = {};

  if (obj.startDate && obj.endDate) {
    objFilter = {
      createdDate: {
        $gte: new Date(+obj.startDate),
        $lt: new Date(+obj.endDate),
      },
    };
  }

  if (obj.startAge || obj.endAge) {
    objFilter["meta.age"] = {
      $gte: +obj.startAge,
      $lt: +obj.endAge,
    };
  }

  if (obj.type) {
    objFilter["meta.personType.id"] = obj.type;
  }

  if (obj.skin) {
    objFilter["meta.skin_color.id"] = obj.skin;
  }

  if (obj.gender) {
    objFilter["meta.gender.id"] = obj.gender;
  }

  // * Update isNotification
  let updateAll = await SnapshotModel.updateMany(
    { isNotification: true },
    { isNotification: false }
  );

  let getFaceModel = await SnapshotModel.find({ ...objFilter }).lean();

  for (const result of getFaceModel) {
    let updateFace = await SnapshotModel.findByIdAndUpdate(result._id, {
      isNotification: true,
    });

    if (!updateFace) {
      //state.logger.error("Unable to update face");
    }
  }

  return true;
}

async function settingLpr(obj: any): Promise<boolean> {
  if (!obj) {
    //state.logger.info("Object is empty");
    return false;
  }

  let _id = new ObjectId("5fa10d1f20cb5050d43c1992");

  let getById = await SettingModel.findById(_id);

  if (getById) {
    let remove = await SettingModel.findByIdAndRemove(_id);

    if (!remove) {
      //state.logger.error("Unable to remove");
      return false;
    }
  }

  obj._id = _id;
  let insert = await SettingModel.collection.insertOne({ ...obj });

  if (!insert) {
    //state.logger.info("Unable to insert setting face");
    return false;
  }

  let objFilter: any = {};

  if (obj.startDate && obj.endDate) {
    objFilter = {
      createdDate: {
        $gte: new Date(+obj.startDate),
        $lt: new Date(+obj.endDate),
      },
    };
  }

  if (obj.country) {
    objFilter.country = obj.country;
  }

  if (obj.make) {
    objFilter.make = obj.make;
  }

  if (obj.model) {
    objFilter.model = obj.model;
  }

  if (obj.vehicleColor) {
    objFilter.vehicleColor = obj.vehicleColor;
  }

  if (obj.vehicleType) {
    objFilter.vehicleType = obj.vehicleType;
  }

  // * Update isNotification
  let updateAll = await LprModel.updateMany(
    { isNotification: true },
    { isNotification: false }
  );

  let getFaceModel = await LprModel.find({ ...objFilter }).lean();

  for (const result of getFaceModel) {
    let updateFace = await LprModel.findByIdAndUpdate(result._id, {
      isNotification: true,
    });

    if (!updateFace) {
      //state.logger.error("Unable to update lpr");
    }
  }

  return true;
}

async function getFaceBySetting(): Promise<any> {
  let getFaceModel = await SnapshotModel.find({
    isNotification: true,
  }).lean();

  return getFaceModel;
}

async function getLprBySetting(): Promise<any> {
  let getLpr = await LprModel.find({
    isNotification: true,
  }).lean();

  return getLpr;
}

async function sendNotification(): Promise<boolean> {
  return true;
}

const settingController = {
  settingFace,
  settingLpr,
  typeSetting,
  getFaceBySetting,
  getLprBySetting,
};

export default settingController;
