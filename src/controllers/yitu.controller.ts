import { TYPES } from "../types";
import IYitu from "../interfaces/yitu.interface";
import container from "../inversify.config";
import IState from "../interfaces/state.interface";
import { Snapshot, SnapshotModel } from "../models/snapshot.model";
import { RealTime, RealTimeModel } from "../models/realTime.model";

let repositories: { [id: number]: string } = {};
let faceIdMapRepoId: { [repoId: string]: number } = {};
var ObjectId = require("mongoose").Types.ObjectId;
let yitu: IYitu;
let state: IState;

async function syncDataFace(
  startDate: number,
  endDate: number
): Promise<Snapshot[] | undefined> {
  yitu = container.get<IYitu>(TYPES.Yitu);
  state = container.get<IState>(TYPES.State);

  await insertRealTime(startDate, endDate);

  // * Snapshot
  let getSnapshot = await yitu.getTrackFace(startDate, endDate);

  if (getSnapshot.data) {
    console.log(getSnapshot.data.length);
    let obj: any[] = [];
    for (const resultFaceImage of getSnapshot.data) {
      let meta = resultFaceImage.meta;
      let name: string = "";
      let skinName: string = "";
      let upperColor: string = "White";
      let typeAge: string = "";
      let upperColorName: string = "";

      if (meta.hairstyle) {
        switch (meta.hairstyle) {
          case 1:
            name = "buzzcut";
            break;
          case 2:
            name = "medium";
            break;
          case 3:
            name = "shaven";
            break;
          case 4:
            name = "short";
            break;
          case 5:
            name = "snood";
            break;
          case 6:
            name = "tress";
            break;
          case 0:
            name = "bald";
            break;
          case -1:
            name = "unknown";
            break;
        }
      }

      if (meta.skin_color) {
        switch (meta.skin_color) {
          case 1:
            skinName = "White";
            break;
          case 2:
            skinName = "Beige";
            break;
          case 3:
            skinName = "Brown";
            break;
          case 4:
            skinName = "Black";
        }
      }

      if (meta.upperbody_color) {
        switch (meta.upperbody_color) {
          case 3:
            upperColorName = "red";
            break;
          case -1:
            upperColorName = "unknown";
            break;
        }
      }

      if (meta.age) {
        switch (meta.age) {
          case 0:
            typeAge = "kid";
            break;
          case 1:
            typeAge = "kid_youth";
            break;
          case 2:
            typeAge = "youth";
            break;
          case 3:
            typeAge = "youth_adult";
            break;
          case 4:
            typeAge = "adult";
            break;
          case 5:
            typeAge = "adult_old";
            break;
          case 6:
            typeAge = "old";
            break;
          case -1:
            typeAge = "unknown";
        }
      }

      obj.push({
        _id: new ObjectId(),
        channel: resultFaceImage.channel_id,
        faceId: +resultFaceImage.face_id,
        faceImageUri: `${process.env.YITU_URL}/vbox/v1/image_storage/http_image?uri=${resultFaceImage.face_image_uri}`,
        screenImageUri: resultFaceImage.scene_image_uri
          ? `${process.env.YITU_URL}/vbox/v1/image_storage/http_image?uri=${resultFaceImage.scene_image_uri}`
          : "",
        createdDate: new Date(+resultFaceImage.timestamp * 1000),
        meta: {
          age: {
            id: meta.age,
            name: typeAge,
          },
          beard: meta.beard,
          calling: meta.calling,
          gender: {
            id: meta.gender,
            name: meta.gender == 0 ? "Female" : "Male",
          },
          glasses: meta.glasses,
          hairstyle: {
            id: meta.hairstyle,
            name: name,
          },
          hat: meta.hat,
          mask: meta.mask,
          quality_score: meta.quality_score,
          sunglass: meta.sunglass,
          skin_color: {
            id: meta.skin_color,
            name: skinName,
          },
          upperbody_color: {
            id: meta.upperbody_color,
            name: meta.upper_body_color,
          },
        },
        personType: {
          id: resultFaceImage.repository_id,
          name: repositories[resultFaceImage.repository_id],
        },
      });
    }

    if (obj.length > 0) {
      let insertFace = await insertSnapshot(obj);

      if (!insertFace) {
        state.logger.info("Unable to insert face");
      }
      state.logger.info("Insert sanpshot");
    }

    return obj;
  }
}

async function alertData(startDate: number, endDate: number) {
  yitu = container.get<IYitu>(TYPES.Yitu);
  state = container.get<IState>(TYPES.State);
  let getSnapshot = await yitu.getTrackFace(startDate, endDate);

  if (!getSnapshot) {
    state.logger.error("Unable to get snapshot");
  }

  state.logger.info("Get snap shot success");
}

async function getDataFace(startDate: Date, endDate: Date, type: number) {
  yitu = container.get<IYitu>(TYPES.Yitu);
  // let getByDate = await SnapshotModel.find({
  //   createdDate: { $gte: startDate, $lte: endDate },
  // })
  //   .sort({ createdDate: -1 })
  //   .lean();

  let lasterDate = undefined;

  let snapShotData: any;

  if (type == 1) {
    snapShotData = await yitu.getFaceInformation(
      +startDate,
      +endDate
      // lasterDate ? +lasterDate.createdDate : +startDate,
      // +endDate
    );
  } else if (type == 2) {
    snapShotData = await yitu.getTrackFace(
      +startDate,
      +endDate
      // lasterDate ? +lasterDate.createdDate : +startDate,
      // +endDate
    );
  }

  let obj: any[] = [];

  /// * TODO Edit response snap shot !!!
  if (snapShotData) {
    if (type == 2) {
      if (Array.isArray(snapShotData.data)) {
        for (const resultFaceImage of snapShotData.data) {
          let meta = resultFaceImage.meta;
          let name: string = "";
          let skinName: string = "";
          let upperColor: string = "White";
          let typeAge: string = "";
          let upperColorName: string = "";

          if (meta.hairstyle) {
            switch (meta.hairstyle) {
              case 1:
                name = "buzzcut";
                break;
              case 2:
                name = "medium";
                break;
              case 3:
                name = "shaven";
                break;
              case 4:
                name = "short";
                break;
              case 5:
                name = "snood";
                break;
              case 6:
                name = "tress";
                break;
              case 0:
                name = "bald";
                break;
              case -1:
                name = "unknown";
                break;
            }
          }

          if (meta.skin_color) {
            switch (meta.skin_color) {
              case 1:
                skinName = "White";
                break;
              case 2:
                skinName = "Beige";
                break;
              case 3:
                skinName = "Brown";
                break;
              case 4:
                skinName = "Black";
            }
          }

          if (meta.upperbody_color) {
            switch (meta.upperbody_color) {
              case 3:
                upperColorName = "red";
                break;
              case -1:
                upperColorName = "unknown";
                break;
            }
          }

          if (meta.age) {
            switch (meta.age) {
              case 0:
                typeAge = "kid";
                break;
              case 1:
                typeAge = "kid_youth";
                break;
              case 2:
                typeAge = "youth";
                break;
              case 3:
                typeAge = "youth_adult";
                break;
              case 4:
                typeAge = "adult";
                break;
              case 5:
                typeAge = "adult_old";
                break;
              case 6:
                typeAge = "old";
                break;
              case -1:
                typeAge = "unknown";
            }
          }

          if (resultFaceImage.timestamp) {
            let timestamp = new Date(+resultFaceImage.timestamp * 1000);
            obj.push({
              _id: new ObjectId(),
              channelName: resultFaceImage.channelName,
              channel: resultFaceImage.channel_id,
              faceId: +resultFaceImage.face_id,
              faceImageUri: `${process.env.YITU_URL}/vbox/v1/image_storage/http_image?uri=${resultFaceImage.face_image_uri}`,
              screenImageUri: resultFaceImage.scene_image_uri
                ? `${process.env.YITU_URL}/vbox/v1/image_storage/http_image?uri=${resultFaceImage.scene_image_uri}`
                : "",
              createdDate: timestamp,
              day: timestamp.getDate(),
              month: timestamp.getMonth(),
              year: timestamp.getFullYear(),

              meta: {
                age: {
                  id: meta.age,
                  name: typeAge,
                },
                beard: meta.beard,
                calling: meta.calling,
                gender: {
                  id: meta.gender,
                  name: meta.gender == 0 ? "Female" : "Male",
                },
                glasses: meta.glasses,
                hairstyle: {
                  id: meta.hairstyle,
                  name: name,
                },
                hat: meta.hat,
                mask: meta.mask,
                quality_score: meta.quality_score,
                sunglass: meta.sunglass,
                skin_color: {
                  id: meta.skin_color,
                  name: skinName,
                },
                upper_body_color: meta.upper_body_color,
              },
              personType: {
                id: resultFaceImage.repository_id,
                name: repositories[resultFaceImage.repository_id],
              },
            });
          }
        }
      }
    }
    if (type == 1) {
      if (Array.isArray(snapShotData)) {
        for (const resultFaceImage of snapShotData) {
          let meta = resultFaceImage.meta;
          let name: string = "";
          let skinName: string = "";
          let upperColor: string = "White";
          let typeAge: string = "";
          let upperColorName: string = "";
          let shirtColor: any[] = [
            "white",
            "red",
            "yellow",
            "blue",
            "black",
            "orange",
          ];

          if (meta.hairstyle) {
            switch (meta.hairstyle) {
              case 1:
                name = "buzzcut";
                break;
              case 2:
                name = "medium";
                break;
              case 3:
                name = "shaven";
                break;
              case 4:
                name = "short";
                break;
              case 5:
                name = "snood";
                break;
              case 6:
                name = "tress";
                break;
              case 0:
                name = "bald";
                break;
              case -1:
                name = "unknown";
                break;
            }
          }

          if (meta.skin_color) {
            switch (meta.skin_color) {
              case 1:
                skinName = "White";
                break;
              case 2:
                skinName = "Beige";
                break;
              case 3:
                skinName = "Brown";
                break;
              case 4:
                skinName = "Black";
            }
          }

          if (meta.upperbody_color) {
            switch (meta.upperbody_color) {
              case 3:
                upperColorName = "red";
                break;
              case -1:
                upperColorName = "unknown";
                break;
            }
          }

          if (meta.age) {
            switch (meta.age) {
              case 0:
                typeAge = "kid";
                break;
              case 1:
                typeAge = "kid_youth";
                break;
              case 2:
                typeAge = "youth";
                break;
              case 3:
                typeAge = "youth_adult";
                break;
              case 4:
                typeAge = "adult";
                break;
              case 5:
                typeAge = "adult_old";
                break;
              case 6:
                typeAge = "old";
                break;
              case -1:
                typeAge = "unknown";
            }
          }

          if (resultFaceImage.createdDate) {
            let timestamp = resultFaceImage.createdDate;

            obj.push({
              _id: new ObjectId(),
              channelName: resultFaceImage.channelName,
              channel: resultFaceImage.channel_id,
              faceId: +resultFaceImage.face_id,
              faceImageUri: resultFaceImage.image,
              screenImageUri: resultFaceImage.scene_image_uri,
              similarity: resultFaceImage.similarity,
              createdDate: timestamp,
              day: timestamp.getDate(),
              month: timestamp.getMonth() + 1,
              year: timestamp.getFullYear(),
              name: resultFaceImage.name,
              personType: resultFaceImage.personType,
              disposition_name: resultFaceImage.disposition_name,
              meta: {
                age: {
                  id: meta.age,
                  name: typeAge,
                },
                beard: meta.beard,
                calling: meta.calling,
                gender: {
                  id: meta.gender,
                  name: meta.gender == 0 ? "Female" : "Male",
                },
                glasses: meta.glasses,
                hairstyle: {
                  id: meta.hairstyle,
                  name: name,
                },
                hat: meta.hat,
                mask: meta.mask,
                quality_score: meta.quality_score,
                sunglass: meta.sunglass,
                skin_color: {
                  id: meta.skin_color,
                  name: skinName,
                },
                upper_body_color: meta.upper_body_color,
              },
            });
          }
        }
      }
    }
  }

  // let index = 1;
  // for (const result of obj) {
  //   let getById = await SnapshotModel.findById(result._id).lean();

  //   if (!getById) {
  //     await SnapshotModel.collection.insertMany(result);
  //   }
  //   console.log(index++);
  // }

  // let a = 0;

  return obj;
}

async function insertSnapshot(obj: Snapshot[]) {
  let insertSnapshot = await SnapshotModel.collection.insertMany(obj);

  if (!insertSnapshot) {
    return;
  }

  return true;
}

async function insertRealTime(startDate: number, endDate: number) {
  let getChannels = await yitu.getChannels(1, +process.env.MAX_SIZE_FACE!);

  if (!getChannels) {
    state.logger.info("Channel is empty");
    return;
  }

  let channel: string = "";
  for (const resultChannel of getChannels) {
    channel += `,${resultChannel.id}`;
  }

  let getRealTime = await yitu.getFaceInformation(startDate, endDate);

  if (getRealTime) {
    let insertRealTime = await RealTimeModel.collection.insertMany(getRealTime);

    if (!insertRealTime) {
      return;
    }
  }

  state.logger.info("Insert realtime");
  return true;
}

const yituController = {
  syncDataFace,
  getDataFace,
  alertData,
};

export default yituController;
