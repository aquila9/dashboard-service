var fs = require("fs");

function base64Encode(file: string) {
  // read binary data
  var bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(bitmap).toString("base64");
}

function fixedNumber(num: number) {
  if (!num) {
    return;
  }

  if (num < 10) {
    return `0${num}`;
  }
}

function setTimeStart(dateTimeNum: number) {
  let dateTime = new Date(dateTimeNum);
  dateTime.setHours(0, 0, 0, 0);

  return dateTime;
}

function setTimeEnd(dateTimeNum: number) {
  let dateTime = new Date(dateTimeNum);
  dateTime.setHours(23, 59, 59, 59);

  return dateTime;
}

const commonController = {
  base64Encode,
  fixedNumber,
  setTimeStart,
  setTimeEnd,
};

export default commonController;
