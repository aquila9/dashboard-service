import axios from "axios";
import { TYPES } from "../types";
import IState from "../interfaces/state.interface";
import container from "../inversify.config";
import settingController from "./setting.controller";

//let state = container.get<IState>(TYPES.State);

function alertLine(type: number, obj: any) {
  if (obj == 0 || !obj) {
    return;
  }

  switch (+type) {
    case settingController.typeSetting.face:
      axios
        .get(`${process.env.LINE_URL}/face?id=${obj}`, {
          timeout: 5 * 1000,
        })
        .then((obj) => {
          console.log("Send line face success");
          //state.logger.info("Send line success");

          return true;
        })
        .catch((err) => {
          // state.logger.error(err.message);
          return false;
          return;
        });
      return;
    case settingController.typeSetting.lpr:
      axios
        .get(`${process.env.LINE_URL}/lpr?id=${obj}`, {
          timeout: 5 * 1000,
        })
        .then((obj) => {
          console.log("Send line lpr success");

          return true;
        })
        .catch((err) => {
          //state.logger.error(err.message);

          return false;
        });
      return;
  }
}

const notificationController = {
  alertLine,
};

export default notificationController;
