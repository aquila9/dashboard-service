import express from "express";
import notificationRouterV1 from "./routes/v1/notification.route"
import rbacRouterV1 from "./routes/v1/rbac.route"
import settingsRouterV1 from "./routes/v1/settings.route"
import channelRouterV1 from "./routes/v1/channel.route"
import lprRouterV1 from "./routes/v1/lpr.route"
import masterRouterV1 from "./routes/v1/master.route"
import authenRouterV1 from "./routes/v1/authen.route";
import faceImageRouterV1 from "./routes/v1/faceImage.route";
import statisticsRouterV1 from "./routes/v1/statistics.route";
import repositoryRouterV1 from "./routes/v1/repository.route";
import resourceRouterV1 from "./routes/v1/resource.route";
import usersRouterV1 from "./routes/v1/users.route";

// Router imports
// End of router imports

var router = express.Router();

// Route routers
// End of routing routers

router.use("/v1/users", usersRouterV1);
router.use("/v1/resource", resourceRouterV1);
router.use("/v1/repository", repositoryRouterV1);
router.use("/v1/statistics", statisticsRouterV1);
router.use("/v1/faceImage", faceImageRouterV1);
router.use("/v1/authen", authenRouterV1);
router.use("/v1/master", masterRouterV1);
router.use("/v1/lpr", lprRouterV1);
router.use("/v1/channel", channelRouterV1);
router.use("/v1/settings", settingsRouterV1);
router.use("/v1/rbac", rbacRouterV1);
router.use("/v1/notification", notificationRouterV1);
export default router;
