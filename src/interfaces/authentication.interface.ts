import winston from "winston";

interface IAuthentication {
  authenJWT(req: any, res: any, next: any): Promise<string>;
}

export default IAuthentication;
