interface ISchedule {
  syncDataFace(cornJob: string): Promise<any>;
  alertFace(cornJob: string): Promise<any>;
  alertLpr(cornJob: string): Promise<any>;
  clearDate(cornJob: string): Promise<any>;
}

export default ISchedule;
