import { UserModel } from "../models/user.model";
import { LoginModel } from "../models/login.model";
import { ResourceModel } from "models/resource.model";
import { FaceMessageModel, RepositoriesModel } from "models/repositories.model";
import { ChannelModel } from "models/channel.model";
import { StatisticsModel } from "models/statistics.model";
import { DataTrackFace, TrackFaceModel } from "models/trackFace.model";
import { HitRealTimeModel } from "models/hitRealTime.model";
import { DispositionModel } from "models/dispostion.model";

export default interface IYitu {
  login(username?: string, password?: string): Promise<LoginModel | undefined>;

  getResource(): Promise<ResourceModel | undefined>;

  getRepositories(): Promise<RepositoriesModel | undefined>;

  getChannels(
    current?: number,
    pageSize?: number
  ): Promise<ChannelModel[] | undefined>;

  getStatistics(
    type: string,
    startDate: number,
    endDate: number,
    timeDim: string
  ): Promise<StatisticsModel | undefined>;

  getTrackImage(
    size?: number,
    offset?: number,
    startDate?: number,
    endDate?: number
  ): Promise<TrackFaceModel | undefined>;

  getRealTimeAlarm(createDate: Date): Promise<HitRealTimeModel | undefined>;

  getFaceInformation(
    startTimeStamp: number,
    endTimeStamp: number
  ): Promise<any | undefined>;

  getAllDisposition(): Promise<DispositionModel[] | undefined>;

  getTrackHitFetch(
    startDate: number,
    endDate: number,
    channel: string,
    disposition: string
  ): Promise<any | undefined>;

  getFaceImage(hitFaceId: string): Promise<any | undefined>;

  getTrackBatch(faceId: string): Promise<any | undefined>;

  getTrackFace(startDate: number, endDate: number): Promise<any | null>;
}
