import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class User extends BaseModel {
  public username!: string;
  public password!: string;
}

export const UserModel = getModelForClass(User, {
  schemaOptions: { collection: "cltUsers" },
});
