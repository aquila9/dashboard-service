import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
import { BaseModel, Pagination } from "./base.model";

export class MetaModel {
  public repo_capacity!: number;
  public type!: number;
}

export class RepositoriesModel extends BaseModel {
  public repositories!: RepositoriesDataModel[];
}

export class RepositoriesDataModel {
  public face_number!: number;
  public id!: number;
  public local!: boolean;
  public meta!: MetaModel;
  public name!: string;
  public summary!: string;
  public sync!: number;
}

export class ListFaceMessage {
  public address!: string;
  public channel_id!: number;
  public face_id!: string;
  public face_image_uri!: string;
  public gender!: string;
  public meta!: any;
  public name!: string;
  public person_id!: string;
  public repository_id!: number;
  public scene_image_uri!: string;
  public timestamp!: TimeStamps;
  public feature!: string;
}

export class FaceMessageModel {
  public list!: ListFaceMessage[];
  public pagination!: Pagination;
}
