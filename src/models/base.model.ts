export class BaseModel {
  public create_time?: Date;
  public update_time?: Date;
  public createdOn?: Date;
}

export class Pagination {
  public total!: number;
  public current!: number;
  public pageSize!: number;
}
