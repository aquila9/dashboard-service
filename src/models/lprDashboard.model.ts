import { getModelForClass, prop } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class LprDashboard {
  @prop({ index: true })
  public eventId!: string;

  @prop()
  public country!: string;

  @prop()
  public make!: string;

  @prop()
  public model!: string;

  @prop()
  public plateNumber!: string;

  @prop({ index: true })
  public createdDate!: Date;

  @prop()
  public vehicleColor!: string;

  @prop()
  public vehicleType!: string;

  @prop()
  public isNotification!: boolean;
}

export const LprDashboardModel = getModelForClass(LprDashboard, {
  schemaOptions: { collection: "cltLprDashboard" },
});
