export class Cpu {
  public idle!: number;
  public total!: number;
  public usage_rate!: number;
}

export class Disk {
  public free!: number;
  public total!: number;
  public usage_rate!: number;
}

export class Ram {
  public free!: number;
  public total!: number;
  public usage_rate!: number;
}

export class Time {
  public begin!: number;
  public duration!: number;
  public now!: number;
}

export class ResourceModel {
  public cpu!: Cpu;
  public disk!: Disk;
  public ram!: Ram;
  public time!: Time;
}
