import { getModelForClass, prop } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";
import { Schema } from "mongoose";

export class Setting extends BaseModel {
  @prop()
  public _id!: Schema.Types.ObjectId;

  @prop()
  public startDate?: string;

  @prop()
  public endDate?: string;

  @prop()
  public age?: string;

  @prop()
  public gender?: string;

  @prop()
  public skin?: string;

  @prop()
  public hairStyle?: string;

  @prop()
  public glass?: string;

  @prop()
  public sunglass?: string;

  @prop()
  public mask?: string;

  @prop()
  public hat?: string;

  @prop()
  public item?: string;

  @prop()
  public type?: string;

  @prop()
  public make?: string;

  @prop()
  public generation?: string;

  @prop()
  public plateNumber?: string;

  @prop()
  public vehicleColor?: string;

  @prop()
  public shirtColor?: string;
}

export const SettingModel = getModelForClass(Setting, {
  schemaOptions: { collection: "cltSetting" },
});
