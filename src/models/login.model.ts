export class LoginModel {
  public access_token!: string;
  public refresh_token!: string;
  public userid!: string;
  public roleid!: string;
}
