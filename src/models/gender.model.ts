import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class Gender extends BaseModel {
  public id!: number;
  public name!: string;
  public description!: string;
}

export const GenderModel = getModelForClass(Gender, {
  schemaOptions: { collection: "cltGender" },
});
