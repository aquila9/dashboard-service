export class HitModel {
  public id!: number;
  public disposition_id!: number;
  public disposition_name!: string;
  public is_hit!: boolean;
  public timestamp!: number;
  public face_id!: string;
  public channel_id!: number;
  public hit_repository_id!: number;
  public hit_face_id!: string;
  public similarity!: number;
  public meta!: any;
}

export class HitRealTimeModel {
  public hits!: HitModel[];
}
