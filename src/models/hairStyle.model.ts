import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class HairStyle extends BaseModel {
  public id!: number;
  public name!: string;
  public description!: string;
}

export const HairStyleModel = getModelForClass(HairStyle, {
  schemaOptions: { collection: "cltHairStyle" },
});
