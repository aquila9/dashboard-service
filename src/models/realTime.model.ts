import { prop, getModelForClass } from "@typegoose/typegoose";

class Gender {
  @prop()
  id!: number;

  @prop()
  name!: string;
}

class HairStyle {
  @prop()
  id!: number;

  @prop()
  name!: string;
}

class PersonType {
  @prop()
  id!: number;

  @prop()
  name!: string;
}

class Skin {
  @prop()
  id!: number;

  @prop()
  name!: string;
}

class Upperbody_color {
  @prop()
  id!: number;

  @prop()
  name!: string;
}

class Meta {
  @prop()
  age!: number;

  @prop()
  beard!: string;

  @prop()
  calling!: number;

  @prop()
  gender!: Gender;

  @prop()
  glasses!: number;

  @prop()
  hairstyle!: HairStyle;

  @prop()
  hat!: number;

  @prop()
  mask!: number;

  @prop()
  quality_score!: number;

  @prop()
  sunglass!: number;

  @prop()
  skin_color!: Skin;

  @prop()
  upperbody_color!: Upperbody_color;
}

export class RealTime {
  @prop()
  channel!: number;

  @prop()
  faceId!: number;

  @prop()
  faceImageUri!: string;

  @prop()
  screenImageUri!: string;

  @prop({ index: true })
  createdDate!: Date;

  @prop()
  meta!: Meta;

  @prop()
  personType!: PersonType;
}

export const RealTimeModel = getModelForClass(RealTime, {
  schemaOptions: { collection: "cltRealTime" },
});
