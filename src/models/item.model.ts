import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class Item extends BaseModel {
  public id!: number;
  public name!: string;
  public description!: string;
}

export const ItemModel = getModelForClass(Item, {
  schemaOptions: { collection: "cltItem" },
});
