import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class Color extends BaseModel {
  public id!: number;
  public name!: string;
  public description!: string;
}

export const ColorModel = getModelForClass(Color, {
  schemaOptions: { collection: "cltColor" },
});
