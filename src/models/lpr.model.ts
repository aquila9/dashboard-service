import { getModelForClass, prop } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class Lpr {
  @prop({ index: true })
  public eventId!: string;

  @prop({ index: true })
  public country!: string;

  @prop({ index: true })
  public make!: string;

  @prop({ index: true })
  public model!: string;

  @prop({ index: true })
  public picture!: string;

  @prop({ index: true })
  public platePicture!: string;

  @prop({ index: true })
  public sceenPicture!: string;

  @prop({ index: true })
  public plateNumber!: string;

  @prop({ index: true })
  public createdDate!: Date;

  @prop({ index: true })
  public vehicleColor!: string;

  @prop({ index: true })
  public vehicleType!: string;

  @prop({ index: true })
  public isNotification!: boolean;

  @prop({ index: true })
  public vehicleList!: string;

  @prop({ index: true })
  public laneName!: string;

  @prop({ index: true })
  public imageSource!: string;
}

export const LprModel = getModelForClass(Lpr, {
  schemaOptions: { collection: "cltLpr" },
});
