import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class Skin extends BaseModel {
  public id!: number;
  public name!: string;
  public description!: string;
}

export const SkinModel = getModelForClass(Skin, {
  schemaOptions: { collection: "cltSkin" },
});
