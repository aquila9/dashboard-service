import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class Age extends BaseModel {
  public id!: number;
  public name!: string;
  public description!: string;
}

export const AgeModel = getModelForClass(Age, {
  schemaOptions: { collection: "cltAge" },
});
