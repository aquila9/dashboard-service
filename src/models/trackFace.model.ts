import { Pagination } from "./base.model";

export class MetaDataTrackFace {
  public age!: number;
  public beard!: number;
  public calling!: number;
  public gender!: number;
  public glasses!: number;
  public hairstyle!: number;
  public hat!: number;
  public kucivilk?: number;
  public mask!: number;
  public quality_score!: number;
  public sunglass!: number;

  // * No data on yitu
  public tel?: string;
  public location?: string;
}

export class DataTrackFace {
  public name!: string;
  public address?: string;
  public face_id!: string;
  public face_image_uri!: string;
  public face_image_content?: string;
  public scene_image_content?: string;
  public meta!: MetaDataTrackFace;
  public timestamp!: number;
  public scene_image_uri!: string;
  public channel_id!: number;
  public repository_id!: number;
  // * Add model from
  public personType?: string;
}

export class TrackFaceModel {
  public data?: DataTrackFace[];
  public pagination!: Pagination;
}
