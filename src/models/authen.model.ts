import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./base.model";

export class Authen extends BaseModel {
  public username!: string;
}

export const AuthenModel = getModelForClass(Authen, {
  schemaOptions: { collection: "cltLoginHistory" },
});
