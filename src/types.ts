const TYPES = {
  // Types
  Schedule: Symbol.for("Schedule"),
  State: Symbol.for("State"),
  Yitu: Symbol.for("Yitu"),
  Authen: Symbol.for("Authen"),
};

export { TYPES };
