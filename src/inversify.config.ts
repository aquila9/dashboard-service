import { Container } from "inversify";
import { TYPES } from "./types";
import ISchedule from "./interfaces/schedule.interface"
import Schedule from "./entities/schedule.entity"
import IState from "./interfaces/state.interface";
import State from "./entities/state.entity";
import IYitu from "./interfaces/yitu.interface";
import Yitu from "./entities/yitu.entity";
import IAuthentication from "interfaces/authentication.interface";
import Authentication from "./entities/authentication.entity";

const container = new Container();
container.bind<IState>(TYPES.State).to(State).inSingletonScope();
container.bind<IYitu>(TYPES.Yitu).to(Yitu).inSingletonScope();
container
  .bind<IAuthentication>(TYPES.Authen)
  .to(Authentication)
  .inSingletonScope();

container.bind<ISchedule>(TYPES.Schedule).to(Schedule).inSingletonScope();
export default container;
