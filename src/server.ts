import cluster from "cluster";
import path from "path";
import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import * as bodyParser from "body-parser";
import { expressMiddleware } from "responsio";
import container from "./inversify.config";
import IState from "./interfaces/state.interface";
import { TYPES } from "./types";
import routers from "./routes";
import IYitu from "interfaces/yitu.interface";
import { HitRealTimeModel } from "models/hitRealTime.model";
import { DataTrackFace, TrackFaceModel } from "models/trackFace.model";
import axios from "axios";
import { RepositoriesModel } from "models/repositories.model";
import { nextTick } from "process";
import { HairStyleModel } from "./models/hairStyle.model";
import { SkinModel } from "./models/skin.model";
import { GenderModel } from "./models/gender.model";
import { ItemModel } from "./models/item.model";
import ISchedule from "interfaces/schedule.interface";
var ip = require("ip");

// * ===== App starts here =====

// * Check .env and assign to process.env
try {
  require("dotenv-safe").config();
} catch (error) {
  console.error(error.message);
  process.exit(error.code || -1);
}

let appEnv = process.env.NODE_ENV || "development";
let state: IState;
let yitu: IYitu;
let schedule: ISchedule;
let mysqlDB: any;

const test = (next: any) => {
  console.log("Hello middle");
  next();
  return "Hello middle";
};
const mysql = require("mysql");

(async () => {
  if (cluster.isMaster) {
    // * Get current cpu count
    const numCPUs = require("os").cpus().length;

    console.log(numCPUs);
    console.log(`Master ${process.pid} started`);

    let maxClusters = Number(process.env.MAX_CLUSTERS || 2);

    let clusters = +numCPUs > maxClusters ? maxClusters : +numCPUs;

    // * Fork workers
    for (let i = 0; i < (appEnv == "production" ? clusters : 1); i++) {
      cluster.fork();
    }

    cluster.on("exit", (worker: any, code: any, signal: any) => {
      console.log(`Worker ${worker.process.pid} died`);
    });
  } else {
    // * Spin up States
    yitu = container.get<IYitu>(TYPES.Yitu);
    state = container.get<IState>(TYPES.State);
    schedule = container.get<ISchedule>(TYPES.Schedule);

    schedule.syncDataFace(`${process.env.TIME_SYNC!}`);
    schedule.alertFace(`${process.env.TIME_ALERT!}`);
    schedule.alertLpr(`${process.env.TIME_ALERT!}`);
    schedule.clearDate(`${process.env.TIME_CLEAR_DATA!}`);

    await connectToDatabaseMongoDB();

    await connectYitu();

    await state.init();

    const app = express();

    app.use(cors());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json({ limit: "1024mb" }));
    app.use(bodyParser.raw());
    app.use(bodyParser.text({ type: "text/plain" }));
    app.use(expressMiddleware());

    app.get("/healthCheck", (req, res) => {
      if (healthCheck()) {
        res.status(200).respond(0, "ok", null);
      } else {
        res.status(500).respond(0, "Unhealthy", null);
      }
    });

    // * Realtime get data for repository
    const http = require("http").createServer(app);

    app.use("/api", routers);

    http.listen(process.env.PORT, () => {
      state.logger.info(
        `Server running on ${ip.address()}:${process.env.PORT}`
      );
    });
  }
})();

async function connectToDatabaseMongoDB() {
  try {
    if (!process.env.CONNECTION_STRING) {
      state.logger.error("Connection string is missing");
      return false;
    }

    state.logger.info(`Connecting to database...`);

    await mongoose.connect(process.env.CONNECTION_STRING, {
      useCreateIndex: true,
      useUnifiedTopology: true,
      useNewUrlParser: true,
      dbName: process.env.DATABASE_NAME,
      useFindAndModify: false,
    });

    state.logger.info(`Database mongoDB connected`);

    return true;
  } catch (error) {
    state.logger.error(`Can't connect to database: ${error.message}`);
    state.logger.error(error);
    return false;
  }
}

async function connectYitu(): Promise<Boolean> {
  yitu = container.get<IYitu>(TYPES.Yitu);
  let login = await yitu.login();

  if (login) {
    state.logger.info(`Connected sucess ${process.env.YITU_URL}`);
    return false;
  }

  state.logger.error(`Cannot Connected sucess ${process.env.YITU_URL}`);
  return true;
}

async function healthCheck(): Promise<Boolean> {
  return true;
}

export default mysqlDB;
